import Api from '@/services/api'

export default {
  fetchDiaries () {
    return Api().get('/diaries')
  },
  postDiary (diary) {
    return Api().post('/diaries', diary,
      { headers: {'Content-type': 'application/json'} })
  },
  upvoteDiary (id) {
    return Api().put(`/diaries/${id}/vote`)
  },
  deleteDiary (id) {
    return Api().delete(`/diaries/${id}`)
  }
}
