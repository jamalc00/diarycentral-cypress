const apiURL = 'https://diarycentral.herokuapp.com/diaries'

describe('diaries page', () => {
  beforeEach(() => {
    // Delete all items in the API's datastore
    cy.request(apiURL)
      .its('body')
      .then(diaries => {
        diaries.forEach(element => {
          cy.request('DELETE', `${apiURL}${element._id}`)
        })
      })
    // Populate API's datastore
    cy.fixture('diaries').then(diaries => {
      let [d1, d2, d3, d4, ...rest] = diaries
      let four = [d1, d2, d3, d4]
      four.forEach(diary => {
        cy.request('POST', apiURL, diary)
      })
    })
    cy.visit('/')
    // Click Report Item in navbar
    cy.get('.navbar-nav')
      .eq(0)
      .find('.nav-item')
      .eq(2)
      .click()
  })
  // Test to add an item
  describe('Add a diary', () => {
    describe('With valid attributes', () => {
      it('allows item to be submitted', () => {
        //  Fill out web form
        cy.get('#type').select('Movie')
        cy.get('#genre').select('Horror')
        cy.get('label')
          .contains('Personal Favorite')
          .next()
          .type('Harry Potter')

        cy.get('input[data-test=stars]').type(5)

        cy.get('label')
          .contains('Personal Comment')
          .next()
          .type('Such an amazing movie!')
        cy.contains('Thanks for your Diary Entry!').should('not.exist')
        cy.get('.error').should('not.exist')
        cy.get('button[type=submit]').click()
        cy.contains('Thanks for your Diary Entry!').should('exist')
      })
    })
  })
})
