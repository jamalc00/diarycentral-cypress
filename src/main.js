// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import * as VueGoogleMaps from 'vue2-google-maps'
import * as firebase from 'firebase'
import VueNoty from 'vuejs-noty'
import 'vuejs-noty/dist/vuejs-noty.css'

Vue.config.productionTip = false

Vue.use(BootstrapVue)
Vue.use(VueNoty)

Vue.component('contact-form', require('./components/ContactUs.vue'))

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyC0s-a9hbB7Fk0TLb35Ig_5mJihJyJkHN4',
    libraries: 'places' // necessary for places input
  }
})

Vue.use(firebase)
var firebaseConfig = {
  apiKey: 'AIzaSyCOhpZv5838J9AOZHYpL4grDcAcRYdL6_A',
  authDomain: 'diarycentral.firebaseapp.com',
  databaseURL: 'https://diarycentral.firebaseio.com',
  projectId: 'diarycentral',
  storageBucket: 'diarycentral.appspot.com',
  messagingSenderId: '466584032714',
  appId: '1:466584032714:web:cd898db01d8ec9933c2b33',
  measurementId: 'G-Z32LRNCT1E'
}

firebase.initializeApp(firebaseConfig)

firebase.analytics()

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
