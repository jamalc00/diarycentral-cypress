# Assignment 2 - Agile Software Practice.

Name: Jamal Cunningham

## Client UI.

>>Home

[Home]: ./images/Home.png

![][Home]

Home page for user interaction

>>Diaries List

[Diaries]: ./images/Diaries.png

![][Diaries]

 Diaries page to show the number of diary entries including details
 
 >>Add Diary
 
 [AddDiary]: ./images/AddDiary.png
 
 ![][AddDiary]

Allows a user to add a diary entry

>>Maps

[Map]: ./images/Map.png

![][Map]

Shows user Organisation Location

>>Register

[Register]: ./images/Register.png

![][Register]

Allows user to register

>>Login

[Login]: ./images/Login.png

![][Login]

Allows user to login

## E2E/Cypress testing.




 (Run Starting)

  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ Cypress:    3.8.1                                                                              │
  │ Browser:    Electron 78 (headless)                                                             │
  │ Specs:      3 found ( home-page.spec.js, diaries.spec.js, login-spec.js)                       │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘


────────────────────────────────────────────────────────────────────────────────────────────────────

  Running:   home-page.spec.js                                                              (1 of 3)
undefined

  Home page
    √ Shows a header (3015ms)
    Navigation bar
      √ Shows the required links (891ms)
      √ Redirects when links are clicked (1666ms)
undefined
undefined
  3 passing (6s)
undefined

  (Results)

  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ Tests:        3                                                                                │
  │ Passing:      3                                                                                │
  │ Failing:      0                                                                                │
  │ Pending:      0                                                                                │
  │ Skipped:      0                                                                                │
  │ Screenshots:  0                                                                                │
  │ Video:        true                                                                             │
  │ Duration:     5 seconds                                                                        │
  │ Spec Ran:      home-page.spec.js                                                               │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘


  (Video)

  -  Started processing:  Compressing to 32 CRF
  -  Finished processing: C:\Users\Jamal\Documents\4th Year\DiaryCentralVue-master\cy    (2 seconds)
                          press\videos\ home-page.spec.js.mp4


────────────────────────────────────────────────────────────────────────────────────────────────────

  Running:  diaries.spec.js                                                                 (2 of 3)
undefined

  diaries page
    Add a diary
      With valid attributes
        1) "before each" hook for "allows item to be submitted"
undefined
undefined
  0 passing (969ms)
  1 failing
undefined
  1) diaries page Add a diary With valid attributes "before each" hook for "allows item to be submitted":
     CypressError: cy.request() failed on:

https://diarycentral.herokuapp.com/diaries5e0ca5f13fdf26396408f932

The response we received from your web server was:

  > 404: Not Found

This was considered a failure because the status code was not '2xx' or '3xx'.

If you do not want status codes to cause failures pass the option: 'failOnStatusCode: false'

-----------------------------------------------------------

The request we sent was:

Method: DELETE
URL: https://diarycentral.herokuapp.com/diaries5e0ca5f13fdf26396408f932
Headers: {
  "Connection": "keep-alive",
  "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Cypress/3.8.1 Chrome/78.0.3904.130 Electron/7.1.4 Safari/537.36",
  "accept": "*/*",
  "accept-encoding": "gzip, deflate",
  "content-length": 0
}

-----------------------------------------------------------

The response we got was:

Status: 404 - Not Found
Headers: {
  "server": "Cowboy",
  "connection": "keep-alive",
  "x-powered-by": "Express",
  "access-control-allow-origin": "*",
  "content-type": "text/html; charset=utf-8",
  "content-length": "41",
  "etag": "W/\"29-F+vfQ3iR6C65WyFaWX3juQacNpg\"",
  "date": "Fri, 03 Jan 2020 05:48:18 GMT",
  "via": "1.1 vegur"
}
Body: <h1>Not Found</h1>
<h2></h2>
<pre></pre>

Because this error occurred during a 'before each' hook we are skipping the remaining tests in the current suite: 'diaries page'
      at Object.cypressErr (https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:86089:11)
      at Object.throwErr (https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:86044:18)
      at Object.throwErrByPath (https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:86076:17)
      at https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:72407:18
      at tryCatcher (https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:120353:23)
      at Promise._settlePromiseFromHandler (https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:118289:31)
      at Promise._settlePromise (https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:118346:18)
      at Promise._settlePromise0 (https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:118391:10)
      at Promise._settlePromises (https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:118470:18)
      at Async../node_modules/bluebird/js/release/async.js.Async._drainQueue (https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:115078:16)
      at Async../node_modules/bluebird/js/release/async.js.Async._drainQueues (https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:115088:10)
      at Async.drainQueues (https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:114962:14)

undefined
undefined

  (Results)

  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ Tests:        1                                                                                │
  │ Passing:      0                                                                                │
  │ Failing:      1                                                                                │
  │ Pending:      0                                                                                │
  │ Skipped:      0                                                                                │
  │ Screenshots:  1                                                                                │
  │ Video:        true                                                                             │
  │ Duration:     0 seconds                                                                        │
  │ Spec Ran:     diaries.spec.js                                                                  │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘


  (Screenshots)

  -  C:\Users\Jamal\Documents\4th Year\DiaryCentralVue-master\cypress\screenshots\dia     (1280x720)
     ries.spec.js\diaries page -- allows item to be submitted -- before each hook (fa
     iled).png


  (Video)

  -  Started processing:  Compressing to 32 CRF
  -  Finished processing: C:\Users\Jamal\Documents\4th Year\DiaryCentralVue-master\cy    (0 seconds)
                          press\videos\diaries.spec.js.mp4


────────────────────────────────────────────────────────────────────────────────────────────────────

  Running:  login-spec.js                                                                   (3 of 3)
undefined

  Login page
    Tests user login
      With valid attributes
        1) allows user to be logged in
        2) "after all" hook for "allows user to be logged in"
undefined
undefined
  0 passing (7s)
  2 failing
undefined
  1) Login page Tests user login With valid attributes allows user to be logged in:
     CypressError: Timed out retrying: Expected to find element: 'input[type=text]', but never found it.
      at Object.cypressErr (https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:86089:11)
      at Object.throwErr (https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:86044:18)
      at Object.throwErrByPath (https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:86076:17)
      at retry (https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:76759:16)
      at onFailFn (https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:65594:16)
      at tryCatcher (https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:120353:23)
      at Promise._settlePromiseFromHandler (https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:118289:31)
      at Promise._settlePromise (https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:118346:18)
      at Promise._settlePromise0 (https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:118391:10)
      at Promise._settlePromises (https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:118466:18)
      at Async../node_modules/bluebird/js/release/async.js.Async._drainQueue (https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:115078:16)
      at Async../node_modules/bluebird/js/release/async.js.Async._drainQueues (https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:115088:10)
      at Async.drainQueues (https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:114962:14)

  2) Login page Tests user login With valid attributes "after all" hook for "allows user to be logged in":
     CypressError: cy.click() failed because it requires a DOM element.

The subject received was:

  > undefined

The previous command that ran was:

  > cy.contains()

Because this error occurred during a 'after all' hook we are skipping the remaining tests in the current suite: 'With valid attributes'
      at Object.cypressErr (https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:86089:11)
      at Object.throwErr (https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:86044:18)
      at Object.throwErrByPath (https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:86076:17)
      at ensureElement (https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:74221:21)
      at validateType (https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:74093:16)
      at Object.ensureSubjectByType (https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:74121:9)
      at pushSubjectAndValidate (https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:81016:15)
      at Context.<anonymous> (https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:81302:18)
      at https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:80781:33
      at tryCatcher (https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:120353:23)
      at Promise._settlePromiseFromHandler (https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:118289:31)
      at Promise._settlePromise (https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:118346:18)
      at Promise._settlePromiseCtx (https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:118383:10)
      at Async../node_modules/bluebird/js/release/async.js.Async._drainQueue (https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:115083:12)
      at Async../node_modules/bluebird/js/release/async.js.Async._drainQueues (https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:115088:10)
      at Async.drainQueues (https://diarycentral.firebaseapp.com/__cypress/runner/cypress_runner.js:114962:14)

undefined
undefined

  (Results)

  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ Tests:        1                                                                                │
  │ Passing:      0                                                                                │
  │ Failing:      1                                                                                │
  │ Pending:      0                                                                                │
  │ Skipped:      0                                                                                │
  │ Screenshots:  2                                                                                │
  │ Video:        true                                                                             │
  │ Duration:     6 seconds                                                                        │
  │ Spec Ran:     login-spec.js                                                                    │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘


  (Screenshots)

  -  C:\Users\Jamal\Documents\4th Year\DiaryCentralVue-master\cypress\screenshots\log     (1280x720)
     in-spec.js\Login page -- Tests user login -- With valid attributes -- allows use
     r to be logged in (failed).png
  -  C:\Users\Jamal\Documents\4th Year\DiaryCentralVue-master\cypress\screenshots\log     (1280x720)
     in-spec.js\Login page -- Tests user login -- With valid attributes -- allows use
     r to be logged in -- after all hook (failed).png


  (Video)

  -  Started processing:  Compressing to 32 CRF
  -  Finished processing: C:\Users\Jamal\Documents\4th Year\DiaryCentralVue-master\cy     (1 second)
                          press\videos\login-spec.js.mp4


====================================================================================================

  (Run Finished)


       Spec                                              Tests  Passing  Failing  Pending  Skipped  
  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ √   home-page.spec.js                       00:05        3        3        -        -        - │
  ├────────────────────────────────────────────────────────────────────────────────────────────────┤
  │ ×  diaries.spec.js                          962ms        1        -        1        -        - │
  ├────────────────────────────────────────────────────────────────────────────────────────────────┤
  │ ×  login-spec.js                            00:06        1        -        1        -        - │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘
    ×  2 of 3 failed (67%)                      00:13        5        3        2        -        -  




