import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Diaries from '../components/Diaries'
import AddDiary from '../components/AddDiary'
import ContactUs from '../components/ContactUs'
import AboutUs from '../components/AboutUs'
import GoogleMaps from '../components/GoogleMaps'
import Login from '../components/Login'
import Register from '../components/Register'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/diaries',
      name: 'Diaries',
      component: Diaries
    },
    {
      path: '/add',
      name: 'AddDiary',
      component: AddDiary
    },
    {
      path: '/about',
      name: 'AboutUs',
      component: AboutUs
    },
    {
      path: '/contact',
      name: 'ContactUs',
      component: ContactUs
    },

    {
      path: '/map',
      name: 'GoogleMaps',
      component: GoogleMaps
    },

    {
      path: '/login',
      name: 'Login',
      component: Login
    },

    {
      path: '/register',
      name: 'Register',
      component: Register
    }

  ]
})
